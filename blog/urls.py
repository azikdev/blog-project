from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.PostListView.as_view(), name='post-list'),
    path('posts/<int:year>/<int:month>/<int:day>/<slug:slug>', views.post_detail_view, name='post-detail'),
    path('posts/<int:post_id>/share/', views.post_share_view, name='post-share')
]
